#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#define THREADS_NUM 5

int shared_resource;
sem_t mutex;

void *thread_routine(void *);

int main(void)
{
	int i,a[THREADS_NUM];
	pthread_t tid[THREADS_NUM];
	
	sem_init(&mutex,0,1);
	
	for(i=0;i<THREADS_NUM;i++)
	{
		a[i]=i;
		pthread_create(&tid[i],NULL,thread_routine,(void *)&a[i]);
	}

	for(i=0;i<THREADS_NUM;i++)
	{
		pthread_join(tid[i],NULL);
	}
}

void *thread_routine(void *num)
{
	int thread_number=*(int *)num;

	printf("\nThread %d: starting...", thread_number);

	sem_wait(&mutex);
	/* Entering critical section */
	printf("\nThread %d: entering critical section", thread_number);
	sleep(2);
	printf("\nThread %d: the shared resource value is: %d", thread_number, shared_resource);
	sleep(1);
	printf("\nThread %d: finishing with shared resource, exiting critical section",thread_number);
	/* Exiting critical section */
	sem_post(&mutex);
}