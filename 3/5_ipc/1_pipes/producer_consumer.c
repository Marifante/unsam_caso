/*
 * Consigna:
 * Muestre con un ejemplo en lenguaje C como realizar un productor consumidor 
 * entre dos procesos utilizando dos pipes. Sugerencia Revise la ayuda 
 * de la llamada al sistema pipe para construir el pipe y de fork 
 * para crear nuevos procesos.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h> // para incluir fork() y pipe()
#include <sys/types.h>
#include <sys/wait.h>

#define PRODUCER_TOTAL_DATA_TO_SEND 2048 /*!< Cantidad total de datos a producir por el productor. */
#define END_OF_TRANSMISSION_MSG "no more data"  /*!< Mensaje enviado por el productor indicando que no hay mas datos para enviar. */

/*
 * @brief rutina implementada por el productor. Escribe una serie
 * de buffers por el pipe para enviarselos al consumidor.
 * @param pipe_write_end el file_descriptor del pipe abierto en
 * escritura.
 */
int producer_routine(FILE *pipe_write_end)
{
    if( NULL == pipe_write_end ) return -1;
    int ret = 0;

    for( int i = 0; i < PRODUCER_TOTAL_DATA_TO_SEND; i++ )
    {
        printf("%s: Se esta generando el dato : 'dato %d'\n", __func__, i);
        if( fprintf(pipe_write_end, "dato %d\n", i) < 0 )
        {
            printf("%s: ERR! El productor no pudo escribir en el pipe!\n", __func__);
            ret = -1;
        }
    }

    if( 1 != fwrite(END_OF_TRANSMISSION_MSG "\n", strlen(END_OF_TRANSMISSION_MSG "\n"), 1, pipe_write_end) )
    {
        printf("%s: ERR! El productor no pudo escribir en el pipe el mensaje de finalizacion!\n", __func__);
        ret = -1;
    }
    fclose(pipe_write_end);
    return ret;
}

/*
 * @brief rutina implementada por el consumidor. Leera por el pipe
 * hasta que llegue un mensaje determinado enviado por el productor.
 * @param pipe_read_end el file_descriptor del pipe abierto en
 * lectura.
 */
int consumer_routine(FILE *pipe_read_end)
{
    if( NULL == pipe_read_end ) return -1;
    int ret = 0;
    bool finish_msg_received = false;
    char received_char;
    char buff[256]; // El consumidor solamente va a guardar el ultimo mensaje recibido en este buffer
    int buff_pointer = 0;

    while( true != finish_msg_received )
    {   // read a line from the pipe
        received_char = (char) fgetc(pipe_read_end);
        if( received_char != EOF )
        {
            if( buff_pointer >= sizeof(buff) )
            {
                printf("%s: ERR! el buffer del consumidor es demasiado pequeño, se limpiara! (%d > %lu)\n", __func__, buff_pointer, sizeof(buff));
                buff_pointer = 0;
            }
            else
            if( received_char == '\n' )
            {   // el caracter de nueva linea delimita los mensajes enviados por el productor
                buff[buff_pointer] = '\0'; // stringify
                printf("%s: Se leyo el dato: %s\n", __func__, buff);
                buff_pointer = 0;
                if( 0 == strcmp(END_OF_TRANSMISSION_MSG, buff) )
                {
                    printf("%s: Se leyo el mensaje de finalizacion! %s\n", __func__, buff);
                    finish_msg_received = true;
                }
            }
            else
            {   
                buff[buff_pointer] = received_char; // guardar el char recibido
                buff_pointer++;
            }
        }
    }
    fclose(pipe_read_end);
    return ret;
}

/*
 * @brief programa principal. Crea el pipe usado por el productor 
 * y el consumidor para comunicarse entre ellos. Luego, crea el proceso
 * consumidor y productor y espera a que finalicen.
 */
int main()
{
    pid_t producer_id, consumer_id;
    int pipe_descriptor[2];
    FILE *pipe_descriptor_write, *pipe_descriptor_read;

    /*
     * El pipe es una via de comunicacion de un solo sentido. Va a ser usado,
     * por el productor para enviarle mensajes al consumidor.
     * El pipe es tratado por los procesos como un "archivo virtual", donde
     * un proceso lee y el otro escribe. Si no hay datos para leer, entonces el
     * proceso consumidor se suspende hasta que los haya. Si no hay espacio para 
     * escribir, entonces el proceso productor se suspende hasta que lo haya.
     */
    if( 0 != pipe(pipe_descriptor) ) // create the pipe
    {
        printf("%s: ERR! No se pudo crear el pipe!\n", __func__);
        return -1;
    }
    else
    {   // Abro el "archivo virtual" 
        pipe_descriptor_read  = fdopen(pipe_descriptor[0], "r");    // para leer (consumidor)
        pipe_descriptor_write = fdopen(pipe_descriptor[1], "w");    // para escribir (productor)

        /* Forkeo para crear el productor. La funcion fork devuelve 0 en el
         * proceso hijo (productor) y el pid del proceso en el proceso padre
         * que lo crea. */
        producer_id = fork();
        if( producer_id == 0 ) 
        {
            fclose(pipe_descriptor_read);
            producer_routine(pipe_descriptor_write);
            return 0;   // el productor retorna
        }
        else
        {
            printf("%s: el pid del productor es %d\n", __func__, producer_id);
        }

        /* Forkeo para crear el consumidor. La funcion fork devuelve 0 en el
         * proceso hijo (consumidor) y el pid del proceso en el proceso padre
         * que lo crea. */
        consumer_id = fork();
        if( consumer_id == 0 ) 
        {
            fclose(pipe_descriptor_write);
            consumer_routine(pipe_descriptor_read);
            return 0;   // el consumidor retorna
        }
        else
        {
            printf("%s: el pid del consumidor es %d\n", __func__, consumer_id);
        }

        // El proceso padre cierra el pipe porque no lo va a usar
        fclose(pipe_descriptor_read);
        fclose(pipe_descriptor_write);

        /* Cada wait() espera a que cualquiera de los dos 
         * procesos hijos termine. Por eso hay dos, uno para cada
         * proceso hijo. */
        wait(NULL);
        wait(NULL);
    }

    return 0;
}
