# Instalación del módulo

Para compilar se usa directamente Make. Por lo tanto, solo hay que ejecutar `make`.
En la compilacion se va a generar un archivo .ko, el cual es un archivo objecto (similar a los .o en C) que posee linkeado algunas estructuras de datos necesarias porel kernel.


Para insertar el modulo en el kernel hay que ejecutar: `insmod hello.ko`
Nota: `insmod` es la herramienta que usa modprobe para insertar modulos. La unica diferencia es que modprobe posee la inteligencia suficiente para resolver dependencias e `insmod` no.

Para remover el modulo del kernel hay que ejecutar: rmmod hello.ko

