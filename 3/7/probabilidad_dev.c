#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/vmalloc.h>
#include <linux/proc_fs.h> /* For proc */
#include <asm/uaccess.h>
#include <linux/module.h> /* For MOD_INC_USE_COUNT, MOD_DEC_USE_COUNT */
#include <linux/time.h>

MODULE_LICENSE("GPL");
/* Macros & Constants ------------------------------------------------------ */
#define MODULE_NAME "probabilidad"  /*!< Name of the module. */
#define RANDOM_LETTERS_QTY 2        /*!< Random letters that will be show to the user when dev is used. */
#define RANDOM_LETTERS_BUFFER_SIZE RANDOM_LETTERS_QTY+2 /*!< The size of the buffer used to send the random letters to the user (the +2 is for a \n\0). */
#define INITIAL_RANDOM_SEED 256

/* Kernel module entry/exit functions -------------------------------------- */
static int __init probability_init(void);
static void __exit probability_exit(void);
/* We inform the kernel that must execute probability_init when the module is 
 * loaded and that must execute probability_exit when the module is unloaded.
 */
module_init(probability_init);
module_exit(probability_exit);

/* Functions Prototypes ---------------------------------------------------- */
long int proc_read_file(struct file *filp, char *buf, size_t count, loff_t *offp );
static ssize_t proc_write_file(struct file *filp, const char __user * buff, size_t count, loff_t *pos);
static int proc_open_file(struct inode *inode, struct file *filp);
static int proc_close_file(struct inode *inode, struct file *filp);

static ssize_t dev_read_file(struct file * file, char * buf, size_t count, loff_t *ppos);
static int dev_open_file(struct inode *inode, struct file *filp);
static int dev_close_file(struct inode *inode, struct file *filp);

unsigned long get_random(unsigned long m_w, unsigned long m_z);

/* Global variables -------------------------------------------------------- */
static int lectures_done = 0;
static struct proc_dir_entry *proc_entry;
static unsigned long prev_random = INITIAL_RANDOM_SEED;
static char *input_seed;
static char lectures_done_str[64];
static char *lectures_done_ptr;
static char random_letters[RANDOM_LETTERS_BUFFER_SIZE];
static char *random_letters_ptr;

/* Operations that will be done when the user interacts with /proc/probabilidad file. */
static const struct file_operations proc_fops = 
{
 	.open = proc_open_file,
	.release = proc_close_file,
	.read = proc_read_file,
	.write = proc_write_file,
};

/* Operations that will be done when the user interacts with /dev/probabilidad file. */
static const struct file_operations probabilidad_fops = 
{
	.open = dev_open_file,
	.release = dev_close_file,
	.read = dev_read_file,
};

/* Device that will be registered when the module is loaded and unregistered
 * when the module is unloaded. The device will be in /sys/class but udev
 * will create it in /dev with its default rules. */
static struct miscdevice probability_dev = 
{
	MISC_DYNAMIC_MINOR,
	MODULE_NAME,
	&probabilidad_fops
};

/* Functions definition ---------------------------------------------------- */
/**
 * @brief it is called when a process tries to open the file, like "cat /proc/probabilidad".
 */
static int proc_open_file(struct inode *inode, struct file *filp)
{
	printk(KERN_INFO MODULE_NAME ": /proc/probabilidad opened...");
	sprintf(lectures_done_str, "%d\n", lectures_done);
	lectures_done_ptr = lectures_done_str;
	return 0;
}

/**
 * @brief it is called when a process closes the file.
 */
static int proc_close_file(struct inode *inode, struct file *filp)
{
	printk(KERN_INFO MODULE_NAME ": /proc/probabilidad closed...");
	return 0;
}

/**
 * @brief show the quantity of lectures to the user. This function is called when
 * the file /proc/probabilidad is read.
 * @param buf is a buffer to fill with data. This buffer will be sent to user.
 * @param buf_length the lengt of the previous buffer.
 * @param offp the offset of the module in the file. 
 * @return 0 if the end of the file is reached (/proc/probabilidad) 
 */
long int proc_read_file(struct file *filp, char *buf, size_t buf_length, loff_t *offp )
{
	long int bytes_read = 0;

	if( *lectures_done_ptr == '\0' )
	{
		bytes_read = 0; // end of file!
	}
	else
	{	// Copy the content of the lectures_done_str and send to the user	
		while( (buf_length > 0) && (*lectures_done_ptr != '\0') )
		{
			put_user( *(lectures_done_ptr++), buf++ );
			buf_length--;
			bytes_read++;
		}
	}
	return bytes_read;
}

/**
 * @brief when we do a 'echo X' in /proc/probabilidad we can set the seed used by the 
 * module. We copy the string introduced by the user in 'input_seed' buffer and we
 * transform it to an unsigned long int.
 */
static ssize_t proc_write_file(struct file *filp, const char __user * buff, size_t count, loff_t *pos)
{
	copy_from_user(input_seed, buff, count); // copy a data block from user space
	prev_random = simple_strtoul(input_seed, NULL, 10);
	printk(KERN_ALERT "%s: New seed introduced: %s", MODULE_NAME, input_seed);
	return count;
}

/**
 * @brief it is called when a process tries to open the file, like "cat /dev/probabilidad".
 * Takes the actual seed, select RANDOM_LETTERS_QTY characters and put it in a buffer.
 */
static int dev_open_file(struct inode *inode, struct file *filp)
{
	unsigned int random_pos;
	char *alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	int i;

	printk(KERN_INFO MODULE_NAME ": /dev/probabilidad opened...");

	for(i=0; i<RANDOM_LETTERS_QTY; i++)
	{
        	prev_random = get_random(prev_random, prev_random);
		random_pos = (prev_random % 25);
		random_letters[i] = alphabet[random_pos];
    	}
	random_letters[i] = '\n';
	random_letters[i+1] = '\0';
	lectures_done++;
	printk(KERN_INFO MODULE_NAME ": random letters chosen: %s", random_letters);
	random_letters_ptr = random_letters;
	return 0;
}

/**
 * @brief it is called when a process closes the file (/dev/probabilidad).
 */
static int dev_close_file(struct inode *inode, struct file *filp)
{
	printk(KERN_INFO MODULE_NAME ": /dev/probabilidad closed...");
	return 0;
}

/**
 * @brief it is called when a process tries to read the file /dev/probabilidad.
 * We send to him the random letters calculated when the file was opened. 
 */
long int dev_read_file(struct file *filp, char *buf, size_t buf_length, loff_t *offp )
{
	long int bytes_read = 0;
	if( *random_letters_ptr == '\0' )
	{
		bytes_read = 0; // end of file!
	}
	else
	{	// Copy the content of the lectures_done_str and send to the user	
		while( (buf_length > 0) && (*random_letters_ptr != '\0') )
		{
			put_user( *(random_letters_ptr++), buf++ );
			buf_length--;
			bytes_read++;
		}
	}
	return bytes_read;
}

/*
 * @brief initial function of the module. It is called when the module is 
 * loaded. 
 */
static int __init probability_init(void)
{
	int ret;
	char *msg = MODULE_NAME ": Initializing module...";

	sprintf(lectures_done_str, "0"); // init in 0 the total lectures done
	lectures_done_ptr = lectures_done_str;

	memset(random_letters, 0, sizeof(random_letters));
	random_letters_ptr = random_letters;

	/*
	 * Create the "probabilidad" device in the /sys/class/misc directory.
	 * Udev will automatically create the device in /dev device using
	 * the default rules.
	 */
	ret = misc_register(&probability_dev);
	if( ret )
	{
		printk(KERN_ERR MODULE_NAME ": Module dispositive could not be registered'\n");
	}
	else
	{
        	// Allocates the buffer where the input seed of the user will be stored
		input_seed = (char *)vmalloc(PAGE_SIZE);
		memset(input_seed, 0, PAGE_SIZE);

        	proc_entry = proc_create_data(MODULE_NAME, 0644, NULL, &proc_fops, msg);

		if(proc_entry == NULL)
		{
			printk(KERN_INFO MODULE_NAME ": entry in /proc could not be created\n");
		}
		printk(KERN_INFO MODULE_NAME ": lectures_done: %s", lectures_done_str);
	}
	return ret;
}

/*
 * @brief removes the proc entry. It is called when the module is unloaded.
 */
static void __exit probability_exit(void)
{
	remove_proc_entry("probabilidad", proc_entry);
	vfree(input_seed);
	misc_deregister(&probability_dev);
}

unsigned long get_random(unsigned long m_w, unsigned long m_z)
{
    m_z = 36969 * (m_z & 65535) + (m_z >> 16);
    m_w = 18000 * (m_w & 65535) + (m_w >> 16);
    return (m_z << 16) + m_w;  /* 32-bit result */
}
