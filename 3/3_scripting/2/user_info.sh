#!/bin/bash

# Escriba un script de shell que tome parámetros desde línea de comandos y 
# genere un archivo de salida en el home del usuario que ejecuta el script 
# con el nombre salida.txt. El contenido de salida.txt dependerá del 
# parámetro indicado en la línea de comando:
# SCRIPT -u : Mostrará la información separada por tabuladores del 
# archivo /etc/passwd correspondiente al usuario indicado o en caso de no 
# existir el usuario “Usuario no encontrado”. En caso de no especificar 
# usuario alguno se mostrará la información de todos los usuarios.
# SCRIPT -g : Mostrará la información de todos los usuarios, separada por 
# tabuladores que pertenezcan al grupo indicado. En caso de no existir 
# el grupo en /etc/group deberá mostrar la leyenda “Grupo no existente”. 
# En caso de existir el grupo pero no tener usuarios asignados, se deberá 
# mostrar la leyendo “Grupo sin usuarios”.

# Funcion utilizada para mostrar por pantalla la informacion de un usuario
function show_user_info(){
	echo "Buscando informacion del usuario ${1}..."
	user_info=`cat /etc/passwd | grep ${1} | sed 's/:/	/g'`
	if [ -z "${user_info}" ]; then
		echo "El usuario ${1} no fue encontrado"
	else
		echo "usuario	clave	UID	GID	nombre	home	interprete"
		echo "${user_info}"
	fi
}

if [ $# -eq 0 ]; then
	echo "No se pasaron opciones, terminando ejecucion..."
	exit 0
fi

# Las opciones del script las leo con getopts y las guardo en la variable
# "opt".
# - Si se ingreso una opcion invalida entonces la variable opt va a estar 
# seteada en "?".
# - Si se usó uno de los parámetros y no se indico un argumento entonces 
# la variable opt se setea en ":".

while getopts ":u:g:" opt; do
  case ${opt} in
	u)
		echo "Ejecutando con parametro (-u)"
		USERNAME=${OPTARG}
		option_used="u"
		break
      	;;
	g)
		echo "Ejecutando con parametro (-g)"
		GROUP=${OPTARG}
		option_used="g"
		break
		;;
 	\?)
		echo "Opcion invalida: -$OPTARG"
		exit 1
		;;
	:)
		if [ ${OPTARG} = "u" ]; then
			ALL_USERS=true 
			echo "La opcion (-u) no recibio ningun argumento, se usara el default (todos los usuarios)..."
			option_used="u"
			break
		else
			echo "La opcion -$OPTARG requiere un argumento."
			exit 1
		fi
      	;;
  esac
done

# Ahora ejecuto la accion correspondiente segun la opcion ingresada
case ${option_used} in
	u)
		if [ ! -z ${ALL_USERS} ]; then
			echo "Ningun usuario fue especificado... mostrando informacion de todos los usuarios..."
			user_info=`cat /etc/passwd | sed 's/:/	/g'`
			echo "usuario	clave	UID	GID	nombre	home	interprete"
			echo "${user_info}"
		elif [ ! -z "${USERNAME}" ]; then
			show_user_info ${USERNAME}
		fi
		;;
	g)
		if [ ! -z "${GROUP}" ]; then
			echo "Buscando usuarios pertenecientes al grupo ${GROUP}..."
			group_info=`sed -n -e /^${GROUP}/p /etc/group`
			if [ -z "${group_info}" ]; then
				echo "El grupo ${GROUP} no fue encontrado"
			else
				group_users=$(echo ${group_info} | cut -d":" -f4 | tr "," "\n")
				if [ -z "${group_users}" ]; then
					echo "El grupo ${GROUP} no tiene usuarios"
				else
					echo "Los usuarios pertenecientes al grupo ${GRO0UP} son:"
					echo ${group_users}
					for user in ${group_users[@]}
					do
						show_user_info ${user}
					done
				fi
			fi
		fi
		;;
esac