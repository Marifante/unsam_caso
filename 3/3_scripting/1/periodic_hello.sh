#!/bin/bash

# Consigna: Escriba un script de shell que cada 5 minutos diga “HOLA” y que 
# todos los días a las 19hrs también los haga. 
# (Pista: busque comandos de control de tiempo para disparar las acciones).

TIME_INTERVAL_S=300	# se imprimira por pantalla 'HOLA' cada 5 minutos
FIXED_HOUR='19:00'	# tambien se imprimira todos los dias a las 19:00 horas

log() {
	echo "[$(date +"%Y-%m-%d %H:%M:%S")] $1"
}


log "initializing periodic_hello.sh..."

# En el while, me fijo dos cosas:
# 1) si pasaron 5 o mas minutos ejecuto "echo 'HOLA'"
# 2) si son las 19:00:00 voy a ejecutar "echo 'HOLA'"
# para trabajar con las fechas, uso el comando date, dando diferentes 
# formatos a la misma y despues parseando el resultado a conveniencia
previous_date=$(date +"%Y-%m-%d %H:%M %s")
previous_day=$(echo ${previous_date} | cut -d" " -f1)
previous_epoch=$(echo ${previous_date} | cut -d" " -f3)
day_alarm_triggered=false
while true
do
	now_date=$(date +"%Y-%m-%d %H:%M %s")
	now_hour=$(echo ${now_date} | cut -d" " -f2)
	now_day=$(echo ${now_date} | cut -d" " -f1)
	now_epoch=$(echo ${now_date} | cut -d" " -f3)

	# esta variable se setea solo al imprimirse la alarma diaria y se renueva dia a dia
	if [ ${now_day} != ${previous_day} ]
	then
		day_alarm_triggered=false
		previous_day=${now_day}
	fi

	if [[ ${now_hour}  == ${FIXED_HOUR} && ${day_alarm_triggered} != true ]]
	then
		day_alarm_triggered=true
		log 'HOLA'
	fi
	
	# se restan el epoch actual y el anterior para verificar si es tiempo de imprimirr
	if [[ $(((10#${now_epoch}-10#${previous_epoch})%${TIME_INTERVAL_S})) == 0  && ${previous_epoch} != ${now_epoch} ]]
	then
		previous_epoch=${now_epoch}
		log 'HOLA'
	fi
done
