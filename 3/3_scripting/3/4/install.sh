#!/bin/bash

echo "Instalando el servicio que imprime 'Chau Mundo' en el apagado..."

sudo cp pre_shutdown_greeting.service /etc/systemd/system/pre_shutdown_greeting.service
sudo systemctl daemon-reload
sudo systemctl enable pre_shutdown_greeting.service
sudo systemctl start pre_shutdown_greeting.service
