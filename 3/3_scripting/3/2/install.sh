#!/bin/bash

echo "Instalando el servicio que imprime 'Buen Dia' al bootear..."

sudo cp boot_greeting.service /etc/systemd/system/boot_greeting.service
sudo systemctl daemon-reload
sudo systemctl enable boot_greeting.service
