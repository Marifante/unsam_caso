#!/bin/bash

if [ -f "/etc/bash.bash_logout" ]
then
	echo "El archivo /etc/bash.bash_logout existe... verificando si existe el comando que imprime el mensaje..."
else
	echo "El archivo /etc/bash.bash_logout no existe... creandolo.."
	sudo /bin/bash -c 'echo "#!/bin/bash" > /etc/bash.bash_logout'
fi

case $(grep -Fx 'echo "Bye Bye"' /etc/bash.bash_logout > /dev/null; echo $?) in
0)
	echo "el mensaje ya esta instalado!"
	;;
1)
	echo "el mensaje no esta instalado! instalando en /etc/bash.bash_logout"
	sudo /bin/bash -c 'echo "echo \"Bye Bye\"" >> /etc/bash.bash_logout'
	;;
*)
	echo "error encontrado en la instalacion!"
	;;
esac
