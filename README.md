# TP Conceptos de Arquitecturas y Sistemas Operativos UNSAM 2021 (2C)

Este repositorio contiene la resolucion de algunas consignas del trabajo practico propuesto por la catedra de Conceptos de Arquitecturas y Sistemas Operativos de la UNSAM en el segundo cuatrimestre del 2021. 

Los integrantes del grupo son:
* Julian Rodriguez (jnrodriguezz@hotmail.com o @marifante en gitlab)
* Federico Posse (federicoperinipose@gmail.com)


El repositorio esta ordenado por consignas. Por ejemplo:

 3
 |_ 3_scripting
 |    |_1 <== ejercicio 3.3.1
 |    |_2 
 |    |_3 <== ejercicio 3.3.3
 |_ 5_ipc <== conjunto de ejercicios 3.5 (IPC)
